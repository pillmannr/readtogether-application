//Answer Collection
Answers2 = new Mongo.Collection("answers2");
var PdfFiles3 = new FS.Collection("pdffiles4", {stores: [new FS.Store.FileSystem("pdffiles4")]});

if (Meteor.isClient) {
	Meteor.subscribe("answers2");
	Meteor.subscribe("pdffiles4");
	Session.set("title", "/tan_book-2.pdf");

	Template.pdfMenuItem.helpers({
  		files: function () {
    		return PdfFiles3.find();
  		}
	});

	Template.body.helpers({
		words: Answers2.find({}, {sort: {createdAt: -1}, limit:20}),
		pdfMenu: PdfFiles3.find({}),
		pdfReaders: [{title: Session.get("title")}]
	});
	
	Template.pdfReaderSpace.helpers({
		title : function(){
			return Session.get("title");
		}
	});

	Template.body.events({
    	"submit .inquiry": function (event) {

		$('#demo').empty();
		$('#answerInformation').empty();
		
    	var word = event.target.text.value;
    	
   		document.getElementById("word").innerHTML = word;
   		var text = word.toLowerCase();
   		var result = Meteor.call("checkDictionary", text, function(error,result){
   			if(error){
   				console.log(error.reason);
   			}
   			else {
   				console.log(result.data.results[1]);
   				var ansTxt = "";
   				//var ansTxt = [];
   				var res = result.data;
    			for (i=0; i<res.total; i++){
    				if ((res.results[i] != undefined) && (res.results[i].senses != undefined) && ((res.results[i].headword == text) || (res.results[i].headword == word))){
    					if (res.results[i].senses[0].definition != undefined){
    						ansTxt = ansTxt + res.results[i].senses[0].definition + " ~~ ";
    					}
    				}
    			}
    			document.getElementById("demo").innerHTML = ansTxt;
   			}
   		
   		});
   		
   		console.log(result);
   		
    	event.target.text.value = "";

		console.log(Answers2.find({wordId:word}));
		var cursor = Answers2.find({wordId:word});
		document.getElementById("tB1").innerHTML = "";

		cursor.fetch().forEach(printToScreen);
		
		function printToScreen (element, index, array) {
			console.log(element.content);
			document.getElementById("tB1").innerHTML = element.content;
		}
		//START
		var newContent = $('.textBox').text();
  			var word = $('#word').text();
  			if(word!=""){
  			//Check if a document already exists in the collection:
  			var count = Answers2.find({wordId:word}).count();
  			  			
  			if (count == 0) {
  				Answers2.insert({wordId: word, content: newContent, createdAt: new Date()});
  			} else {
  				//Answers.update({wordId: word}, {$set : {content: content}});
  				Meteor.call("updateWord", word, newContent, function(error,result){
  					if(error){
  						console.log(error.reason);
  					}
  				});
  			}
  		}
  			$(".textBox").empty();
  		//END

    	return false;
  		},
  		
  		"dropped #dropzone":function(event, temp){
  			console.log('files dropped');
  			FS.Utility.eachFile(event, function (file) {
  				PdfFiles3.insert(file, function (err, fileObj){
  				});
  			});
  		},
  		  		
  		"click #saveAnswer": function() {
  			var newContent = $('.textBox').text();
  			var word = $('#word').text();
  			if(word!=""){
  			//Check if a document already exists in the collection:
  			var count = Answers2.find({wordId:word}).count();
  			  			
  			if (count == 0) {
  				Answers2.insert({wordId: word, content: newContent, createdAt: new Date()});
  			} else {
  				//Answers.update({wordId: word}, {$set : {content: content}});
  				Meteor.call("updateWord", word, newContent, function(error,result){
  					if(error){
  						console.log(error.reason);
  					}
  				});
  			}
  			}
  			$(".textBox").empty();
  		}
	});
	
	/*Template.pdfMenuItem.events({
		'click .wordByName':function(event){
			console.log("Event wordByName reached");
			var name = this._id;
			var newFile = PdfFiles.find({_id: name});
			console.log(PdfFiles.find({_id: name}));
			newTitle = newFile._transform;
			document.getElementById("bookTitle").innerHTML = name;
			Session.set("title", newTitle);
			console.log("THE TITLE:   "+Session.get("title"));
		},
	});*/
	
	Template.word.events({
		'click .list-group-item':function(event){
			console.log(this.wordId);
			var word = this.wordId;
    	
   		document.getElementById("word").innerHTML = word;
   		var text = word.toLowerCase();
   		var result = Meteor.call("checkDictionary", text, function(error,result){
   			if(error){
   				console.log(error.reason);
   			}
   			else {
   				console.log(result.data.results[1]);
   				var ansTxt = "";
   				//var ansTxt = [];
   				var res = result.data;
    			for (i=0; i<res.total; i++){
    				if ((res.results[i] != undefined) && (res.results[i].senses != undefined) && ((res.results[i].headword == text) || (res.results[i].headword == word))){
    					if (res.results[i].senses[0].definition != undefined){
    						ansTxt = ansTxt + res.results[i].senses[0].definition + "; ";
    					}
    				}
    			}
    			document.getElementById("demo").innerHTML = ansTxt;
   			}
   		
   		});
   		
   		console.log(result);

		console.log(Answers2.find({wordId:word}));
		var cursor = Answers2.find({wordId:word});
		console.log("CURSOR" + cursor.fetch());

		cursor.fetch().forEach(printToScreen);
		
		function printToScreen (element, index, array) {
			document.getElementById("tB1").innerHTML = element.content;
		}

    	return false;
  		}
	});
	
	Accounts.ui.config({
  		passwordSignupFields: "USERNAME_ONLY"
	});

if (Meteor.isServer) {

  	Meteor.startup(function () {
    // code to run on server at startup
  	});
  	Meteor.methods({
  		checkDictionary: function (text) {
  			console.log("FIRST STEP DICTIONARY");
            this.unblock();
            try {
            	var result = HTTP.get("https://api.pearson.com/v2/dictionaries/ldoce5/entries?headword="+text+"&apikey=I3Kgb5V1JZvF7cLkLD9kEqgEWdGuDfgp");
            	return true;
            } catch(e) {
            	return false;
            }
        }
    });
}

}
